﻿using UnityEngine;
using System.Collections;

public class CarManager : MonoBehaviour {

    public float minSpawnTime = 2;
    public float maxSpawnTime = 5;
    public Object prefabCar;

    public Transform endDestination;

    protected float chosenSpawnTime;
    protected float currentSpawnTime;

	// Use this for initialization
	void Start ()
    {
        resetTime();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!Pause.scene_pause)
        {
            currentSpawnTime += Time.deltaTime;
            if (currentSpawnTime > chosenSpawnTime)
            {
                GameObject obj = (GameObject)Instantiate(prefabCar, transform.position, transform.rotation);
                StatePatternCar spcar = obj.GetComponent<StatePatternCar>();
                spcar.setDestination(endDestination);
                spcar.init();
                resetTime();
            }
        }
    }

    void resetTime()
    {
        currentSpawnTime = 0;
        chosenSpawnTime = Random.Range(minSpawnTime, maxSpawnTime);
    }
}
