﻿using UnityEngine;
using System.Collections;

public abstract class CarState 
{
    protected readonly StatePatternCar car;

    public CarState(StatePatternCar patternCar)
    {
        car = patternCar;
    }

    public abstract void UpdateOnce();

    public abstract void UpdateState();

    public abstract void Stop();
}

