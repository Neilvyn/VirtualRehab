﻿using UnityEngine;
using System.Collections;

public class TriggerPause : MonoBehaviour
{

	public GameObject pedestrianRedLight;
    public GameObject carRedLight;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        GameObject objectEntering = other.gameObject;
        
        if(objectEntering.tag == "Pedestrian")
        {
            if (pedestrianRedLight.activeSelf == true) {

                var p =  objectEntering.GetComponent<CowboyNavigation>();
                p.pause(objectEntering.transform.position);
                /*CowboyNavigation[] cN = MonoBehaviour.FindObjectsOfType<CowboyNavigation>();
				foreach (var p in cN) {
                    if ()
                    {
                        p.pause(objectEntering.transform.position);
                    }
				}*/

            }
        }
        else if (objectEntering.tag == "Car")
        {
            if (carRedLight.activeSelf == true)
            {
                var car = objectEntering.GetComponent<StatePatternCar>();
                car.pause(objectEntering.transform.position);

              /*  StatePatternCar[] carControllers = MonoBehaviour.FindObjectsOfType<StatePatternCar>();
                foreach (var car in carControllers)
                {
                    car.pause(objectEntering.transform.position);
                }*/
            }
        }
    }
}
