﻿using UnityEngine;
using System.Collections;

public class collisionSpartan : MonoBehaviour {
	public AudioSource[] sounds;
	public AudioSource noise1;
	public AudioSource noise2;
	//public Pause pause;
	// Use this for initialization
	void Start () {
		sounds = GetComponents<AudioSource>();
		noise1 = sounds[0];
		noise2 = sounds[1];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter (Collision col)
	{
		GetComponent<Animation>().Play("diehard");
		GetComponent<AudioSource>().Stop ();
		//GetComponent<AudioSource> ().clip = voice_male_01_hurt_01;
		noise2.Play();
		//GetComponent<AudioSource> ().PlayOneShot (voice_male_01_hurt_01,1);
		Pause.scene_pause = true;
	}
}
