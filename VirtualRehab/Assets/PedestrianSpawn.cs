﻿using UnityEngine;
using System.Collections;

public class PedestrianSpawn : MonoBehaviour {

    public GameObject prefabs;
    uint nbPedestrians = 0;
    static uint maxPedestrians = 20;

    protected GameObject[] spawnPoints;
    public Camera playerHead;

    public float timeMin;
    public float timeMax;

    private float currentTime;
    private float timeToWait;

    protected void spawn(GameObject spawnPos)
    {
        if (nbPedestrians >= maxPedestrians)
        {
            return;
        }

        Vector3 screenPoint = playerHead.WorldToViewportPoint(spawnPos.transform.position);
        bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;

        if (!onScreen)
        {
            Instantiate(prefabs, spawnPos.transform.position, spawnPos.transform.rotation);
            nbPedestrians++;
        }
    }

    // Use this for initialization
    void Start () {
        spawnPoints = GameObject.FindGameObjectsWithTag("Spawn");
        int nbSpawnPts = spawnPoints.Length;

        currentTime = 0;
        timeToWait = Random.Range(timeMin, timeMax);
    }
	
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime;
        if (currentTime > timeToWait)
        {
            int index = Random.Range(0, spawnPoints.Length);

            spawn(spawnPoints[index]);

            currentTime = 0;
            timeToWait = Random.Range(timeMin, timeMax);
        }
	}

    public void Decrease()
    {
        nbPedestrians--;
    }
}
