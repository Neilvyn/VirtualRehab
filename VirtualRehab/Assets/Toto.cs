﻿using UnityEngine;
using System.Collections;

public class Toto : MonoBehaviour {

	// pedestrian light
	public GameObject redLightMat;
	public GameObject greenLightMat;

	// car light
	public GameObject CarRedLightOn;
	public GameObject CarRedLightOff;
	public GameObject CarOrangeLightOn;
	public GameObject CarOrangeLightOff;
	public GameObject CarGreenLightOn;
	public GameObject CarGreenLightOff;

	public CowboyNavigation[] cN;

    public float TimerGreen;


	float m_compteur = 0;

	// Use this for initialization
	void Start () { 
	}
	
	// Update is called once per frame
	void Update () {
		if (m_compteur < 50) {
			redLightMat.SetActive (true);
			greenLightMat.SetActive (false);

			// car 
			CarGreenLightOff.SetActive (false);
			CarGreenLightOn.SetActive (true);

			CarOrangeLightOff.SetActive(true);
			CarOrangeLightOn.SetActive (false);

			CarRedLightOff.SetActive (true);
			CarRedLightOn.SetActive (false);

            var carControllers = MonoBehaviour.FindObjectsOfType<StatePatternCar>();
            foreach (var car in carControllers)
            {
                car.GotoDest();
            }

        } else if (m_compteur > 90 && m_compteur < 100) {
			
			CarOrangeLightOff.SetActive(false);
			CarOrangeLightOn.SetActive (true);

			CarGreenLightOff.SetActive(true);
			CarGreenLightOn.SetActive (false);

			CarRedLightOff.SetActive (true);
			CarRedLightOn.SetActive (false);
		}

		else if (m_compteur > 100){

			// green Pedestrian
			redLightMat.SetActive (false);
			greenLightMat.SetActive (true);


			CarGreenLightOff.SetActive(true);
			CarGreenLightOn.SetActive (false);

			CarOrangeLightOff.SetActive(true);
			CarOrangeLightOn.SetActive (false);

			CarRedLightOff.SetActive (false);
			CarRedLightOn.SetActive (true);

			cN = MonoBehaviour.FindObjectsOfType<CowboyNavigation>();
			foreach (var p in cN) {
				p.GotoDest ();
			}


		}
		if (m_compteur > 200)
			m_compteur = 0;
		m_compteur+=Time.deltaTime*10;
	}
}
