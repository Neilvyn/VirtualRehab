﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	public static bool scene_pause;
	// Use this for initialization
	void Start () {
		scene_pause = false;
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            scene_pause = !scene_pause;

        }
        if (scene_pause)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
