﻿using UnityEngine;
using System.Collections;

public class StatePatternCar : MonoBehaviour {

    [HideInInspector]
    public NavMeshAgent navMeshAgent;

    [HideInInspector]
    public CarState currentCarState;
    [HideInInspector]
    public DrivingState carDrivingState;

    [HideInInspector]
    public Vector3 destination;

    [HideInInspector]
    public BoxCollider collider;


    // Use this for initialization
    void Start () {
        navMeshAgent = GetComponent<NavMeshAgent>();
        collider = GetComponent<BoxCollider>();

        carDrivingState = new DrivingState(this);
        currentCarState = carDrivingState;

        currentCarState.UpdateOnce();
    }

    public void init()
    {
        //currentCarState.UpdateOnce();
    }

    public void setDestination(Transform transform)
    {
        destination = transform.position;
    }

    // Update is called once per frame
    void Update () {
        if (!Pause.scene_pause)
        {
            currentCarState.UpdateState();
        }
    }

    public void stopCar()
    {
        currentCarState.Stop();
    }

    public void restartCar()
    {
        currentCarState.UpdateOnce();
    }

    public void GotoDest()
    {
        //navMeshAgent.SetDestination(destination);
        restartCar();
    }

    public void pause(Vector3 pos)
    {
        //navMeshAgent.SetDestination(pos);
        stopCar();
    }

    public void OnTriggerEnter(Collider other)
    {
        GameObject gameObject = other.gameObject;
        if(gameObject.tag == "Car")
        {
            stopCar();
        }
    }
    public void OnTriggerExit(Collider other)
    {
        restartCar();
    }

}
