﻿using UnityEngine;
using System.Collections;

public class CowboyNavigation : MonoBehaviour {

    protected NavMeshAgent navMeshAgent;
    protected Animator animator;
    protected Vector3 destination;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();

        GameObject[] destinationPoints = GameObject.FindGameObjectsWithTag("Despawn");
        int nbDests = destinationPoints.Length;

        destination = destinationPoints[Random.Range(0, nbDests)].transform.position;

        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.SetDestination(destination);
    }
	
	// Update is called once per frame
	void Update () {
		if(navMeshAgent.remainingDistance < 1)
        {
            animator.SetTrigger("TurnIdle");
        }
	}

	public void GotoDest()
	{
        navMeshAgent.SetDestination (destination);
		animator.SetTrigger ("WalkAgain");
    }

	public void pause(Vector3 pos)
	{
        navMeshAgent.SetDestination(pos);
        animator.SetTrigger("TurnIdle");
    }
}
