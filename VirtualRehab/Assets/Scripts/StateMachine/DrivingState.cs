﻿using UnityEngine;
using System.Collections;


public class DrivingState : CarState
{

        private bool dontdoit;
        private bool initdone;

        public DrivingState(StatePatternCar patternCar) 
            : base(patternCar)
        {
        initdone = false;
    }

  
        public override void UpdateOnce()
        {
            if (!initdone)
            {
                // GameObject[] cargoals = GameObject.FindGameObjectsWithTag("CarGoal");
                // GameObject cargoal = cargoals[Random.Range(0,cargoals.Length-1)];
                car.navMeshAgent.SetDestination(car.destination);
                dontdoit = false;
                initdone = true;
            }
        }

        public override void UpdateState()
        {
            initdone = false;
            if (car.navMeshAgent.remainingDistance < 1 && !dontdoit)
            {
                GameObject.Destroy(car.gameObject);
            }  
        }

        public override void Stop()
        {
            car.navMeshAgent.Stop();
            dontdoit = true;
        }

}

