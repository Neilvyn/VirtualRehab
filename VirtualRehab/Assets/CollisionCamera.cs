﻿using UnityEngine;
using System.Collections;

public class CollisionCamera : MonoBehaviour {
    public AudioSource[] sounds;
    public AudioSource noise1;
    public AudioSource noise2;
    // Use this for initialization
    void Start () {
        sounds = GetComponents<AudioSource>();
        noise1 = sounds[0];
        noise2 = sounds[1];
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter (Collision col)
	{
        //GetComponent<Animation>().Play("diehard");
        GetComponent<AudioSource>().Stop();
        //GetComponent<AudioSource> ().clip = voice_male_01_hurt_01;
        noise2.Play();
        Pause.scene_pause = true;
    }
}
