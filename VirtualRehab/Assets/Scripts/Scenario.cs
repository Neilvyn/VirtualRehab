﻿using UnityEngine;
using System.Collections;

public class Scenario : MonoBehaviour {

    public enum Scenario_type{HOUSE=0,
        SIDEWALK=1,
        CROSSWALK=2,
        CORNER = 3,
        END =4
    };

    public GameObject user;

    public Scenario_type startScenario;
    public Transform[] positionScenario;

    protected Scenario_type currentScenario;

    // Use this for initialization
    void Start () {
        currentScenario = startScenario;
        ChangeScenario(startScenario);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            currentScenario = Scenario_type.HOUSE;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            currentScenario = Scenario_type.SIDEWALK;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            currentScenario = Scenario_type.CROSSWALK;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            currentScenario = Scenario_type.CORNER;
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            currentScenario = Scenario_type.END;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            currentScenario++;
            if((int)currentScenario >= System.Enum.GetNames(typeof(Scenario_type)).Length)
            {
                currentScenario = Scenario_type.HOUSE;
            }
        }


        ChangeScenario(currentScenario);
    }

    void ChangeScenario(Scenario_type scenario)
    {
        var numberScenarios = System.Enum.GetNames(typeof(Scenario_type)).Length;
        if (numberScenarios == positionScenario.Length)
        {
            int index = (int)scenario;
            user.transform.position = positionScenario[index].position;
            user.transform.rotation = positionScenario[index].rotation;
        }
    }
}
